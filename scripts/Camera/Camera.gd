extends Camera2D

export var min_zoom : float
export var max_zoom : float
export var speed : float
var _pan : bool = false
var _zoom_target : float = 1.0
var _old_zoom : float = 1.0

func _input(event):
	if current:
		if event is InputEventMouseButton:
			if event.pressed && event.button_index == BUTTON_WHEEL_DOWN:
				_zoom_camera(false)
			if event.pressed && event.button_index == BUTTON_WHEEL_UP:
				_zoom_camera(true)
				
		if event is InputEventMouseMotion:
			if (_pan):
				offset -= event.relative * zoom
		
		if Input.is_action_just_pressed("middle_click"):
			_pan = true
		if Input.is_action_just_released("middle_click"):
			_pan = false

func _process(delta):
	if _pan:
		Input.set_default_cursor_shape(Input.CURSOR_DRAG)
	else:
		Input.set_default_cursor_shape(Input.CURSOR_ARROW)

func _zoom_camera(in_ : bool) -> void:
	var dir : int					# zoom direction
	if in_:
		dir = -1
	else:
		dir = 1
	_zoom_target += speed * dir
	_zoom_target = clamp(_zoom_target, max_zoom, min_zoom)
	if _zoom_target != max_zoom || _zoom_target != max_zoom:
		offset += _get_camera_offset()
	zoom = Vector2(_zoom_target, _zoom_target)
	_old_zoom = _zoom_target

func _get_camera_offset() -> Vector2:
	var m : Vector2 = get_global_mouse_position()
	var k : Vector2 = global_position
	return ((k - m) * _zoom_target / _old_zoom + m) - k