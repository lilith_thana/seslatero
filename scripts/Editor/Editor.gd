extends Node2D

const UNTITLED_MAP : String = "Untitled"
const UNSAVED_MAP : String = "*"

var hex_map : PackedScene = preload("res://scenes/HexMap.tscn")
var current_map : HexMap setget _set_current_map

func _ready():
	# connect the tool bar buttons to the editor
	
	# iterate through the file menu first
	var container : VBoxContainer = get_node("UILayer/ToolBar/PanelContainer/HBoxContainer/MainMenu/File/Menu/MarginContainer/VBoxContainer")
	for button in container.get_children():
		# Skip non-button classes
		if !button.is_class("Button"):
			continue
		assert(has_method(str("_on_", button.name, "_pressed")))
		var e : int = button.connect("pressed", self, str("_on_", button.name, "_pressed"))
		assert(e == OK)
	
	# connect NewHexMapWindow to Editor
	var e : int = $UILayer/NewHexMapWindow.connect("new_map_created", self, "_on_new_map_created")
	assert(e == OK)
	
	# connect TabBar to Editor
	e = $UILayer/TabBar/Tabs.connect("tab_changed", self, "_on_tab_changed")
	assert(e == OK)
	
	e = $UILayer/TabBar/Tabs.connect("reposition_active_tab_request", self, "_on_tab_reposition")
	assert(e == OK)
	
	e = $UILayer/TabBar/Tabs.connect("tab_close", self, "_on_tab_close")
	
	# create a default map and add it to the Editor
	var map := {"map_size" : Vector2(10,10), "hex_size" : 50, "orientation" : 0, "side" : 0}
	#make_map(map)

func _on_New_pressed():
	$UILayer/NewHexMapWindow.popup_centered()
	$UILayer/ToolBar.show_menu("PanelContainer/HBoxContainer/MainMenu/File/Menu", false)
	
	
func _on_Open_pressed():
	pass
	
	
func _on_Save_pressed():	
	pass
	
	
func _on_SaveAs_pressed():
	pass
	
	
func _on_Exit_pressed():
	pass
	
	
func _on_new_map_created(map : Dictionary):
	make_map(map)
	

func _on_tab_changed(tab : int):
	_set_current_map($Maps.get_child(tab))
	
	
func _on_tab_reposition(idx_to : int):
	$Maps.move_child(current_map, idx_to)
	

func _on_tab_close(tab : int):
	$UILayer/TabBar/Tabs.remove_tab(tab)
	close_map($Maps.get_child(tab))

func make_map(map : Dictionary):
	var h_map = hex_map.instance()
	if !map.has("title"):
		map["title"] = check_duplicate_title(UNTITLED_MAP) + UNSAVED_MAP
	$Maps.add_child(h_map)
	h_map.build(map)
	$UILayer/TabBar/Tabs.add_tab(h_map.title)
	h_map.get_node("Camera2D").current = true
	_set_current_map(h_map)
	
	
# Checks for a duplicate title. If it finds one, it increments its suffix value by one higher than the higest value found.
func check_duplicate_title(title : String) -> String:
	if $Maps.get_child_count() == 0:
		return title
		
	var a : int = 0
	var regex = RegEx.new()
	regex.compile("[0-9]+$")
	for t in $Maps.get_children():
		# Strip star from end of title
		var t_ : String = str(t.title.rstrip("*"))
		# Check if title contains ending numbers
		var result : RegExMatch = regex.search(t_)
		if result:
			# Check if ending number is higher than current count if so, make that the current count
			if int(result.get_string()) > a:
				a = int(result.get_string())
			# Remove the numbers from the end of the string
			t_ = t_.rstrip(result.get_string())
		# Increment count
		if t_ == title:
			a += 1
	# If there is a count return the title with that as a suffix
	if a > 0:
		return title + str(a)
	return title


func close_map(m : HexMap) -> void:
	# TODO: Check if map is unsaved and offer to save it. If Untitled, the offer to save as.
	m.queue_free()


func _set_current_map(m : HexMap) -> void:
	if current_map != null:
		current_map.visible = false
	current_map = m
	current_map.visible = true
	$UILayer/TabBar/Tabs.current_tab = current_map.get_index()
