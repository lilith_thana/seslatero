extends Popup

signal new_map_created(map)

var orientation : int
var shape : int = 0
var layout_menu : int = 0
var layout_selection : int = 0


func _on_CloseButton_pressed() -> void:
	hide()


func _on_HSlider_value_changed(value : float) -> void:
	$MarginContainer/VBoxContainer/HexSizeMarginContainer/HexSizeHBoxContainer/LineEdit.text = str(value)


func _on_LineEdit_text_entered(new_text : String) -> void:
	$MarginContainer/VBoxContainer/HexSizeMarginContainer/HexSizeHBoxContainer/HSlider.value = float(new_text)


func _create_map() -> void:
	var map := {}
	var size : Vector2
	var hex_size : int
	var radius : int
	
	size.x = float($MarginContainer/VBoxContainer/MapSizeMarginContainer/MapSizeHBoxContainer/XContainer/LineEdit.text)
	size.y = float($MarginContainer/VBoxContainer/MapSizeMarginContainer/MapSizeHBoxContainer/YContainer/LineEdit.text)
	radius = int($MarginContainer/VBoxContainer/MapSizeMarginContainer/MapSizeHBoxContainer/RadiusContainer/LineEdit.text)
	hex_size = int($MarginContainer/VBoxContainer/HexSizeMarginContainer/HexSizeHBoxContainer/LineEdit.text)
	
	assert(size != Vector2.ZERO)
	assert(hex_size != 0)
	assert(radius != 0)
	
	map = {"map_size" : size, "hex_size" : hex_size, "orientation" : orientation, "shape" : shape, "layout" : layout_selection, "radius" : radius}
	emit_signal("new_map_created", map)
	
	
func _on_Pointy_pressed() -> void:
	if orientation != 0:
		orientation = 0
		_change_layout_menu(layout_menu - 4)
	
	
func _on_Flat_pressed() -> void:
	if orientation != 1:
		orientation = 1
		_change_layout_menu(layout_menu + 4)


func _on_Accept_pressed() -> void:
	_create_map()
	hide()


func _on_Shapes_item_selected(ID) -> void:
	if ID == 1:
		$MarginContainer/VBoxContainer/MapSizeMarginContainer/MapSizeHBoxContainer/XContainer.visible = false
		$MarginContainer/VBoxContainer/MapSizeMarginContainer/MapSizeHBoxContainer/YContainer.visible = false
		$MarginContainer/VBoxContainer/MapSizeMarginContainer/MapSizeHBoxContainer/RadiusContainer.visible = true
	else:
		$MarginContainer/VBoxContainer/MapSizeMarginContainer/MapSizeHBoxContainer/XContainer.visible = true
		$MarginContainer/VBoxContainer/MapSizeMarginContainer/MapSizeHBoxContainer/YContainer.visible = true
		$MarginContainer/VBoxContainer/MapSizeMarginContainer/MapSizeHBoxContainer/RadiusContainer.visible = false
	ID += orientation * 4
	_change_layout_menu(ID)
	


func _change_layout_menu(ID : int) -> void:	
	assert(ID <= 7)
	$MarginContainer/VBoxContainer/LayoutMarginContainer/LayoutHBoxContainer.get_child(layout_menu).visible = false
	layout_menu = ID
	$MarginContainer/VBoxContainer/LayoutMarginContainer/LayoutHBoxContainer.get_child(layout_menu).visible = true
	shape = ID % 4


func _on_LayoutItemSelected(ID):
	layout_selection = ID
