extends Control

var _menu_clicked := false
var _current_menu := null

func _ready() -> void:
	var e : int
	e = $PanelContainer/HBoxContainer/MainMenu/File.connect("pressed", self, "_on_file_button_pressed")
	assert(e == OK)
	e = $PanelContainer/HBoxContainer/MainMenu/File.connect("mouse_entered", self, "_on_file_mouse_entered")
	assert(e == OK)
	e = $PanelContainer/HBoxContainer/MainMenu/Edit.connect("pressed", self, "_on_edit_button_pressed")
	assert(e == OK)
	e = $PanelContainer/HBoxContainer/MainMenu/Edit.connect("mouse_entered", self, "_on_edit_mouse_entered")
	assert(e == OK)
	e = $PanelContainer/HBoxContainer/MainMenu/Help.connect("pressed", self, "_on_help_button_pressed")
	assert(e == OK)
	e = $PanelContainer/HBoxContainer/MainMenu/Help.connect("mouse_entered", self, "_on_help_mouse_entered")
	assert(e == OK)
	
	$PanelContainer/HBoxContainer/MainMenu/File/Menu.set_process_input(false);
	$PanelContainer/HBoxContainer/MainMenu/Edit/Menu.set_process_input(false);
	$PanelContainer/HBoxContainer/MainMenu/Help/Menu.set_process_input(false);
	

func _input(event : InputEvent) -> void:
	if event is InputEventMouseButton:
		if (event.pressed && _menu_clicked):
			var ev_local : InputEventMouseButton = make_input_local(event)
			var r1 := Rect2(Vector2.ZERO, _current_menu.rect_size)
			if !r1.has_point(ev_local.position):
				for b in get_node("PanelContainer/HBoxContainer/MainMenu").get_children():
					var r2 := Rect2(Vector2.ZERO, b.rect_size)
					if !r2.has_point(ev_local.position):
						_menu_clicked = false
						_current_menu.hide()
						_current_menu = null
						return


func _on_file_button_pressed() -> void:
	_menu_clicked = !_menu_clicked
	if _menu_clicked:
		show_menu("PanelContainer/HBoxContainer/MainMenu/File/Menu", true)
	else:
		show_menu("PanelContainer/HBoxContainer/MainMenu/File/Menu", false)


func _on_edit_button_pressed() -> void:
	_menu_clicked = !_menu_clicked
	if _menu_clicked:
		show_menu("PanelContainer/HBoxContainer/MainMenu/Edit/Menu", true)
	else:
		show_menu("PanelContainer/HBoxContainer/MainMenu/Edit/Menu", false)


func _on_help_button_pressed() -> void:
	_menu_clicked = !_menu_clicked
	if _menu_clicked:
		show_menu("PanelContainer/HBoxContainer/MainMenu/Help/Menu", true)
	else:
		show_menu("PanelContainer/HBoxContainer/MainMenu/Help/Menu", false)


func _on_file_mouse_entered() -> void:
	if _menu_clicked:
		_change_menu("File")


func _on_edit_mouse_entered() -> void:
	if _menu_clicked:
		_change_menu("Edit")
		

func _on_help_mouse_entered() -> void:
	if _menu_clicked:
		_change_menu("Help")


func _change_menu(n : String) -> void:
	var mMenu : HBoxContainer = get_node("PanelContainer/HBoxContainer/MainMenu")
	for child in mMenu.get_children():
		assert(has_node("PanelContainer/HBoxContainer/MainMenu/" + child.name + "/Menu"))
		if child.name == n:
			show_menu("PanelContainer/HBoxContainer/MainMenu/" + child.name + "/Menu", true)
		else:
			show_menu("PanelContainer/HBoxContainer/MainMenu/" + child.name + "/Menu", false)


func show_menu(path : String, value : bool) -> void:
	var p : PanelContainer = get_node(path)
	p.visible = value
	if value:
		_current_menu = p
	p.set_process_input(value)