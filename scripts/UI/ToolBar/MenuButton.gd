extends Button

func _ready():
	var e : int
	e = connect("mouse_entered", self, "_on_mouse_entered")
	assert(e == OK)
	e = connect("mouse_exited", self, "_on_mouse_exited")
	assert(e == OK)
	

func _on_mouse_entered():
	$Label.add_color_override("font_color", Color("#e35a78"))
	
func _on_mouse_exited():
	$Label.add_color_override("font_color", Color("#949aa8"))