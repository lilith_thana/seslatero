extends Node2D

class_name Hex

var q : int
var r : int
var s : int

var hover : bool = false
var selected : bool = false

signal selected(hex)

func init(q : int, r : int):
	self.q = q
	self.r = r
	self.s = -q - r
	assert(q + r + s == 0)
	
	$Label.bbcode_text = str("[center][color=#6600ffff]",q, "[/color], [color=#66ff00ff]", r, "[/color], [color=#66ffff00]", s, "[/color][/center]")
	$Label.rect_position.x -= $Label.rect_size.x / 2


func _input(event):
	if hover:
		if event is InputEventMouseButton && !event.is_echo() && event.pressed && event.button_index == BUTTON_LEFT:
			emit_signal("selected", self)
			get_tree().set_input_as_handled()
		if event is InputEventMouseButton && !event.is_echo() && !event.pressed && event.button_index == BUTTON_LEFT:
			get_tree().set_input_as_handled()

func _draw():
	if hover:
		draw_colored_polygon($CellArea/CollisionPolygon2D.polygon, Color(0.84, 0.93, 0.98, .5))
	if selected:
		draw_colored_polygon($CellArea/CollisionPolygon2D.polygon, Color("88e35a78"))
		
func _process(delta):
	update()
	

func set_collision_area(ps : Array):
	var ps_ := []
	for p in ps:
		ps_.append(to_local(p))
	$CellArea/CollisionPolygon2D.polygon = ps_
	


func _on_CellArea_mouse_entered():
	hover = true


func _on_CellArea_mouse_exited():
	hover = false


func _on_CellArea_input_event(viewport, event, shape_idx):
	if event is InputEventMouseButton && !event.is_echo() && event.pressed && event.button_index == BUTTON_LEFT:
		emit_signal("selected", self)
	get_tree().set_input_as_handled()
