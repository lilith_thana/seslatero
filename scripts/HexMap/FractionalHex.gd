extends Node2D

class_name FractionalHex

var q : float
var r : float
var s : float

func _init(q_ : float, r_ : float):
	q = q_
	r = r_
	s = -q_ - r_
	
