extends Node2D

class_name HexHelper

var hex_directions := [Hex.new(1, 0, -1), Hex.new(1, -1, 0), Hex.new(0, -1, 1), Hex.new(-1, 0, 1), 
					   Hex.new(-1, 1, 0), Hex.new(0, 1, -1)]

func equals(a, b) -> bool:
	return a.q == b.q && a.r == b.r && a.s == b.s


func add(a : Hex, b : Hex) -> Hex:
	return Hex.new(a.q + b.q, a.r + b.r)


func subtract(a : Hex, b : Hex) -> Hex:
	return Hex.new(a.q - b.q, a.r - b.r)


func mutliply(a : Hex, b : Hex) -> Hex:
	return Hex.new(a.q * b.q, a.r * b.r)


func length(hex : Hex) -> int:
	return int((abs(hex.q) + abs(hex.r) + abs(hex.s) / 2))


func distance(a : Hex, b : Hex) -> int:
	return length(subtract(a,b))


func direction(direction : int) -> Hex:
	assert( 0 <= direction && direction < 6)
	return hex_directions[direction]


func neighbor(hex : Hex, direction : int) -> Hex:
	return add(hex, direction(direction))
