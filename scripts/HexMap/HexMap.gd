extends Node2D

class_name HexMap

var map_size : Vector2
var hex_size : int
var rows : int
var title : String
var map := {}
var orientation : Orientation
var size : Vector2
var origin : Vector2
var shape : int
var layout : int
var radius : int
var selected_hex : Hex

var hex : PackedScene = preload("res://scenes/Hex.tscn")

var LAYOUT_POINTY := Orientation.new(sqrt(3.0), sqrt(3.0) / 2.0, 0.0, 3.0 / 2.0, sqrt(3.0) / 3.0, -1.0 / 3.0, 0.0, 2.0 / 3.0, 0.5)
var LAYOUT_FLAT := Orientation.new(3.0 / 2.0, 0.0, sqrt(3.0) / 2.0, sqrt(3.0), 2.0 / 3.0, 0.0, -1.0 / 3.0, sqrt(3.0) / 3.0, 0.0)


func _unhandled_input(event):
	if event is InputEventMouseButton && event.pressed && !event.is_echo() && event.button_index == BUTTON_LEFT:
		selected_hex.selected = false
		selected_hex = null


func build(map_ : Dictionary) -> void:
	assert(map_.has("title"))
	title = String(map_["title"])
	map_size = map_["map_size"]
	hex_size = int(map_["hex_size"])
	if int(map_["orientation"] == 0):
		orientation = LAYOUT_POINTY
	else:
		orientation = LAYOUT_FLAT
	shape = int(map_["shape"])
	layout = int(map_["layout"])
	origin = Vector2(get_viewport_rect().size.x / 2, get_viewport_rect().size.y / 2)
	radius = int(map_["radius"])
	match shape:
		# rectangle
		0:
			map = _make_rectangle()
		# hexagon
		1:
			map = _make_hexagon()
		# parellelogram
		2:
			map = _make_parallelogram()
		# triangles
		3:
			map = _make_triangle()
	_set_cell_collision_data()
	_connect_cells_to_map()

func _make_parallelogram() -> Dictionary:
	var map := {}
	var q1 : int
	var q2 : int
	var r1 : int
	var r2 : int
	q1 = int(-floor(map_size.x / 2))
	r1 = int(-floor(map_size.y / 2))
	q2 = int(floor(map_size.x / 2)) + (int(map_size.x) & 1)
	r2 = int(floor(map_size.y / 2)) + (int(map_size.y) & 1)
	
	for q in range(q1, q2):
		for r in range(r1, r2):
			var h = hex.instance()
			if orientation == LAYOUT_POINTY:
				match layout:
					0:
						h.init(q, r)
						map[Vector2(q, r)] = h
						$Cells.add_child(h)
					1:
						h.init(-q-r, r)
						map[Vector2(-q-r, r)] = h
						$Cells.add_child(h)
					2:
						h.init(r, -q-r)
						map[Vector2(r, -q-r)] = h
						$Cells.add_child(h)
			else:
				match layout:
					0:
						h.init(q, r)
						map[Vector2(q, r)] = h
						$Cells.add_child(h)
					1:
						h.init(r, -q-r)
						map[Vector2(r, -q-r)] = h
						$Cells.add_child(h)
					2:
						h.init(-q-r, r)
						map[Vector2(-q-r, r)] = h
						$Cells.add_child(h)
						
	return map


func _make_triangle() -> Dictionary:
	var map := {}
	
	match layout:
		0:
			for q in range(0, map_size.x):
				for r in range(0, map_size.y - q):
					var h = hex.instance()
					h.init(q,r)
					map[Vector2(q, r)] = h
					$Cells.add_child(h)
		1:
			for q in range(0, map_size.x):
				for r in range(map_size.y - q, map_size.y):
					var h = hex.instance()
					h.init(q,r)
					map[Vector2(q, r)] = h
					$Cells.add_child(h)
	return map


func _make_hexagon() -> Dictionary:
	var map := {}
	var radius = floor(map_size.x / 2)
	for q in range(-radius, radius + 1):
		var r1 := max(-radius, -q - radius)
		var r2 := min(radius, -q + radius)
		for r in range(r1, r2 + 1):
			var h = hex.instance()
			h.init(q,r)
			map[Vector2(q, r)] = h
			$Cells.add_child(h)
	return map


func _make_rectangle() -> Dictionary:
	var map := {}
	
	for r in range(0, map_size.y):
		var r_offset = floor(r / 2)
		for q in range(-r_offset, map_size.x - r_offset):
			var h = hex.instance()
			if orientation == LAYOUT_POINTY:
				match layout:
					0:
						# pointy even normal
						h.init(q,r)
						map[Vector2(q, r)] = h
						$Cells.add_child(h)
					1:
						# pointy even tilt left
						h.init(-q-r,q)
						map[Vector2(-q-r, q)] = h
						$Cells.add_child(h)
					2:
						# pointy even tilt right
						h.init(r, -q-r)
						map[Vector2(r, -q-r)] = h
						$Cells.add_child(h)
					3:
						# pointy odd normal
						h.init(-q-r,r)
						map[Vector2(-q-r, r)] = h
						$Cells.add_child(h)
					4:
						# pointy odd tilt left
						h.init(q, -q-r)
						map[Vector2(q, -q-r)] = h
						$Cells.add_child(h)
					5:
						# pointy odd tilt right
						h.init(r, q)
						map[Vector2(r, q)] = h
						$Cells.add_child(h)
			else:
				match layout:
					0:
						# flat even normal
						h.init(r, q)
						map[Vector2(r, q)] = h
						$Cells.add_child(h)
					1:
						# flat even tilt left
						h.init(-q-r, r)
						map[Vector2(-q-r, r)] = h
						$Cells.add_child(h)
					2:
						# flat even tilt right
						h.init(q, -q-r)
						map[Vector2(q, -q-r)] = h
						$Cells.add_child(h)
					3:
						# flat odd normal
						h.init(r, -q-r)
						map[Vector2(r, -q-r)] = h
						$Cells.add_child(h)
					4:
						# flat odd tilt left
						h.init(-q-r, q)
						map[Vector2(-q-r, q)] = h
						$Cells.add_child(h)
					5:
						# flat odd tilt right
						h.init(q, r)
						map[Vector2(q, r)] = h
						$Cells.add_child(h)
	return map


func _set_cell_collision_data() -> void:
	for h in map.values():
		var ps = polygon_corners(h)
		var center : Vector2 = hex_to_pixel(h)
		h.global_position = center
		h.set_collision_area(ps)
		
func _connect_cells_to_map() -> void:
	for h in map.values():
		h.connect("selected", self, "on_cell_selected")

func _draw():
	for h in map.values():
		var ps = polygon_corners(h)
		var center : Vector2 = h.global_position
		var offset : Vector2 = hex_corner_offset(0)
		ps.append(Vector2(center.x + offset.x, center.y + offset.y))
		draw_polyline(ps, Settings.hex_grid_color, 1)


func on_cell_selected(cell : Hex) -> void:
	if selected_hex:
		selected_hex.selected = false
	selected_hex = cell
	selected_hex.selected = true


func hex_to_pixel(h : Hex) -> Vector2:
	var M : Orientation = orientation
	var x : float = (M.f0 * h.q + M.f1 * h.r) * hex_size
	var y : float = (M.f2 * h.q + M.f3 * h.r) * hex_size
	return Vector2(x + origin.x, y + origin.y)
	
	
func pixel_to_hex(p : Vector2) -> FractionalHex:
	var M : Orientation = orientation
	var pt := Vector2((p.x - origin.x) / hex_size, (p.y - origin.y) / hex_size)
	var q : float = M.b0 * pt.x + M.b1 * pt.y
	var r : float = M.b2 * pt.x + M.b3 * pt.y
	return FractionalHex.new(q, r)
	

func hex_corner_offset(corner : int) -> Vector2:
	var angle : float = 2.0 * PI * (orientation.start_angle + corner) / 6
	return Vector2(hex_size * cos(angle), hex_size * sin(angle))
	
	
func polygon_corners(h : Hex) -> Array:
	var corners := []
	var center : Vector2 = hex_to_pixel(h)
	for i in range(6):
		var offset : Vector2 = hex_corner_offset(i)
		corners.append(Vector2(center.x + offset.x, center.y + offset.y))
	return corners